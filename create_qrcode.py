import qrcode

MAX_TEXT_SIZE = 20


def create_qr_image(txt):
    qr = qrcode.QRCode(
        box_size=MAX_TEXT_SIZE,
    )
    qr.add_data(txt)
    qr.make(fit=True)
    return qr.make_image(fill_color="black", back_color="white")


text = 'qrcode alimentiamoci'
img = create_qr_image(text)
img.save('qrcode.jpg', 'JPEG')
