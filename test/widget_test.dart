import 'package:flutter_app/main.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Title is Home', (WidgetTester tester) async {
    await tester.pumpWidget(MyApp());

    final titleFinder = find.text('Home');
    expect(titleFinder, findsOneWidget);
  });
}
