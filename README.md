# flutter_app
A demo app with:
* a listing
* a qr reader

## Lib info
* barcode
  * [qr_mobile_vision](https://pub.dev/packages/qr_mobile_vision)
  * (se funzionasse sarebbe android, iOs, web) [qrcode_reader](https://pub.dev/packages/qrcode_reader)
* calendar
  * [add_2_calendar](https://pub.dev/packages/add_2_calendar#-example-tab-) (android, iOs, web)
* web
  * [http](https://pub.dev/packages/http)

## Fake api
* [Fake api](https://jsonplaceholder.typicode.com/)

##  Generate Qrcode
```
pip install qrcode
python create_qrcode.py
```

## Test
* [Testing](https://flutter.dev/docs/cookbook/testing)
* [Test on Device](https://flutter.dev/docs/cookbook/testing/integration/introduction)
```bash
flutter drive --target=test_driver/app.dart
```

# Web Support
* [guide](https://flutter.dev/docs/get-started/web)
```
flutter config --enable-web
flutter create .
# restart the ide
flutter run -d chrome
```
* [If web disable some features](https://stackoverflow.com/questions/60184085/how-can-i-disable-some-functionality-only-if-web-on-flutter-app)

# TODO
* provare compilazione iOs [settings iOs](https://flutter.dev/docs/get-started/install/macos)
