import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ApiSample extends StatefulWidget {
  @override
  ApiSampleState createState() => ApiSampleState();
}

class ApiSampleState extends State<ApiSample> {
  String textInfo = "Api wait...";
  final url = 'https://jsonplaceholder.typicode.com/todos/1';

  Future<void> _call() async {
    var response = await http.get(url);
    setState(() {
      textInfo = response.body;
    });
  }

  void _clean() {
    setState(() {
      textInfo = 'Api wait...';
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Api test'),
      ),
      body: Column(
        children: [
          Text(
            textInfo,
            style: TextStyle(fontSize: 20.0),
          ),
          FlatButton(
              color: Colors.blue,
              textColor: Colors.white,
              disabledColor: Colors.grey,
              disabledTextColor: Colors.black,
              padding: EdgeInsets.all(8.0),
              splashColor: Colors.blueAccent,
              onPressed: () {
                _clean();
              },
              child: Text(
                "Clean",
                style: TextStyle(fontSize: 20.0),
              )),
          FlatButton(
              color: Colors.blue,
              textColor: Colors.white,
              disabledColor: Colors.grey,
              disabledTextColor: Colors.black,
              padding: EdgeInsets.all(8.0),
              splashColor: Colors.blueAccent,
              onPressed: () {
                _call();
              },
              child: Text(
                "Call",
                style: TextStyle(fontSize: 20.0),
              )),
        ],
      ),
    );
  }
}
